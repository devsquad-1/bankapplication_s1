import React, { useState } from "react";
//import users from "./users";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Optimum DigiBank
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const schema = yup.object().shape({
  password: yup
    .string()
    .required("Please enter your password!")
    .matches(
      /^(?=.*[a-z])(?=.*\d)(?=.*[@$!%*#?&])[a-z\d@$!%*#?&]{6,}$/,
      "Must Contain 6 Characters, One Number and one special case Character!"
    ),
  retypedpassword: yup
    .string()
    .required("Please re-enter your password!")
    .oneOf([yup.ref("password"), null], "Password must match!"),
});

export default function ResetPassword() {
  const classes = useStyles();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const history = useHistory();

  const [success, setSuccess] = useState(false);
  const [title, setTitle] = useState("Enter New Password");
  const [errMessage, setErrMessage] = useState("");

  async function onSubmit(formValue) {
    const newPassword = formValue.password;
    const newRetypedPassword = formValue.retypedpassword;
    const userID = window.localStorage.getItem("id");

    try {
      const response = await axios.get(`http://localhost:8000/users/${userID}`);
      const oldUser = response.data;

      if (oldUser.password === newPassword) {
        setErrMessage("Sorry, you are unable to reuse an old password!");
      } else {
        const newUser = {
          ...oldUser,
          password: newPassword,
          retypedpassword: newRetypedPassword,
        };

        await axios.put(`http://localhost:8000/users/${userID}`, newUser);
        setTitle("Success");
        setTimeout(() => {
          history.push("/login");
        }, 3000);
        setSuccess(true);
        window.localStorage.clear();
      }
    } catch (err) {
      console.log('Email Not Found')
    }
  }
  return (
    <div>
      <br />
      <br />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            {success ? <ThumbUpIcon /> : <LockOutlinedIcon />}
          </Avatar>
          <Typography component="h1" variant="h5">
            {title}
          </Typography>
          {success ? (
            <div>
              <br />
              <p style={{ color: "green", textAlign: "center" }}>
                Password Successfully Reset!
              </p>
            </div>
          ) : (
            <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  {...register("password")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.password?.message}
                </p>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="retypedpassword"
                  label="Retype Password"
                  type="password"
                  id="retypedpassword"
                  autoComplete="current-password"
                  {...register("retypedpassword")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.retypedpassword?.message}
                </p>
                {errMessage === "" ? (
                  ""
                ) : (
                  <p style={{ color: "red", textAlign: "center" }}>
                    {errMessage}
                  </p>
                )}
              </Grid>

              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Submit
              </Button>
            </form>
          )}
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    </div>
  );
}
