import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/login">
      Optimum DigiBank
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const schema = yup.object().shape({
  firstname: yup.string().required("First Name is a required field!"),
  lastname: yup.string().required("Last Name is a required field!"),
  emailaddress: yup
    .string()
    .email("Email Address must be a valid email!")
    .required("Email Address is a required field!"),
  password: yup
    .string()
    .required("Please enter your password!")
    .matches(
      /^(?=.*[a-z])(?=.*\d)(?=.*[@$!%*#?&])[a-z\d@$!%*#?&]{6,}$/,
      "Must Contain 6 Characters, One Number and one special case Character!"
    ),
  retypedpassword: yup
    .string()
    .required("Please re-enter your password!")
    .oneOf([yup.ref("password"), null], "Password must match!"),
});

export default function Register() {
  const classes = useStyles();
  const [errMessage, setErrMessage] = useState("");
  console.log('component mounted')

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });
  const history = useHistory();

  async function onSubmit(formValue) {
    const userPasswordInput = formValue.password;
    const userRetypedPasswordInput = formValue.retypedpassword;
    const userEmailAddressInput = formValue.emailaddress;

    const newUser = {
      ...formValue,
      accountbalance: 0,
      status: false,
    };

    console.log(formValue);
    setErrMessage("");
    const existingUser = await axios.get(
      `http://localhost:8000/users?emailaddress=${userEmailAddressInput}`
    );
    console.log(existingUser);
    if (existingUser.data.length > 0) {
      setErrMessage("Email Address already exists!");
      setTimeout(()=>setErrMessage(""),2000);
    } else {
      if (userPasswordInput === userRetypedPasswordInput) {
        const response = await axios.post(
          "http://localhost:8000/users",
          newUser
        );
        console.log(response);
        history.push("/success");
      }
    }
  }

  return (
    <div>
      <br />
      <br />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  {...register("firstname")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.firstname?.message}
                </p>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                  {...register("lastname")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.lastname?.message}
                </p>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  {...register("emailaddress")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.emailaddress?.message}
                </p>
                {errMessage === "" ? (
                  ""
                ) : (
                  <p style={{ color: "red", textAlign: "center" }}>
                    {errMessage}
                  </p>
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  {...register("password")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.password?.message}
                </p>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="retypedpassword"
                  label="Retype Password"
                  type="password"
                  id="retypedpassword"
                  autoComplete="current-password"
                  {...register("retypedpassword")}
                />
                <p style={{ color: "red", textAlign: "center" }}>
                  {errors.retypedpassword?.message}
                </p>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    </div>
  );
}
