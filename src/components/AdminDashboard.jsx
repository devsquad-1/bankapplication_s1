import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function AdminDashboard() {
  const history = useHistory();

  const [refresh, setRefesh] = useState(true);
  const classes = useStyles();
  const [users, setUsers] = useState([]);
  const handleLogout = () => {
    history.push("/admin-logout");
  };
  const getAllUsers = async () => {
    const response = await axios.get("http://localhost:8000/users");
    setUsers(response.data);
  };
  useEffect(() => {
    getAllUsers();
  }, [refresh]);

  const handleClick = async (id) => {
    //const getUser = await axios.get(`http://localhost:8000/users/${id}`);
    const getUser = users.filter((user) => {
      return user.id === id;
    });
    const newUser = {
      ...getUser[0],
      status: !getUser[0].status,
    };
    axios.put(`http://localhost:8000/users/${id}`, newUser);
    setRefesh(!refresh);
  };

  return (
    <div>
      <div style={{ marginBottom: 50 }}></div>
      <Grid
        spacing={3}
        direction="column"
        container
        alignItems="center"
        justify="center"
        style={{ marginRight: "50px" }}
      >
        <Grid item xs={12}>
        <Typography color="primary" gutterBottom variant="h5" align="center">ADMIN DASHBOARD</Typography>
          <TableContainer component={Paper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">ID</StyledTableCell>
                  <StyledTableCell align="center">Name</StyledTableCell>
                  <StyledTableCell align="center">Email</StyledTableCell>
                  <StyledTableCell align="center">
                    Account Balance
                  </StyledTableCell>
                  <StyledTableCell align="center">Status</StyledTableCell>
                  <StyledTableCell align="center">Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((user) => (
                  <StyledTableRow key={user.id}>
                    <StyledTableCell align="center">{user.id}</StyledTableCell>
                    <StyledTableCell align="center">
                      {user.firstname} {user.lastname}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {user.emailaddress}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      ${user.accountbalance}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {user.status ? "Active" : "Inactive"}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <Button
                        variant="contained"
                        color={user.status === true ? "secondary" : "primary"}
                        onClick={() => handleClick(user.id)}
                      >
                        {user.status === true ? "Deactivate" : "Activate"}
                      </Button>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <Grid container alignItems="center" justify="center">
          <Grid>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleLogout}
            >
              Logout
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
