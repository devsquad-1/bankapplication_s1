import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { useHistory } from "react-router-dom";

export default function Logout() {
  const history = useHistory();
  useEffect(() => {
    setTimeout(() => {
      history.push("/login");
      window.localStorage.clear();
    }, 2000);
  }, [history]);
  return (
    <div>
      <Container
        component="main"
        maxWidth="xs"
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography align="center" color="primary" variant="h3">
              Logout
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography align="center" color="textSecondary" variant="body1">
              Thank you for banking with us! See you soon!
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
