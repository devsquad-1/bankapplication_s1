import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { useHistory } from "react-router-dom";

export default function SuccessRegister() {
  const history = useHistory();
  useEffect(() => {
    setTimeout(() => {
      history.push("/login");
      window.localStorage.clear();
    }, 5000);
  }, [history]);
  return (
    <div>
      <Container
        component="main"
        maxWidth="xs"
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography align="center" color="primary" variant="h4">
            Email successfully registered!
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography align="center" color="textSecondary" variant="body1">
            Please wait 1-2 business days for admins to activate your account.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
