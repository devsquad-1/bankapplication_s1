import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function UserDashboard() {
  const classes = useStyles();
  const [currentUser, setUser] = useState({});
  const history = useHistory();
  const handleLogout = () => {
    history.push("/logout");
  };
  const getUserDetails = async () => {
    const id = window.localStorage.getItem("userID");
    const response = await axios.get(`http://localhost:8000/users?id=${id}`);
    console.log(response);
    setUser(response.data[0]);
  };
  useEffect(() => {
    getUserDetails();
    //console.log(userDB.userDetails.AccountBalance);
  }, []);

  return (
    <div>
      <div style={{ marginBottom: 50 }}></div>
      <Grid
        spacing={3}
        direction="column"
        container
        alignItems="center"
        justify="center"
        style={{ marginRight: "50px" }}
      >
        <Grid item xs={12}>
          <Typography color="primary" gutterBottom variant="h5" align="center">MY ACCOUNT</Typography>
          <TableContainer component={Paper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Name</StyledTableCell>
                  <StyledTableCell align="center">Email</StyledTableCell>
                  <StyledTableCell align="center">Account Balance</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                  <StyledTableRow key={currentUser.id}>
                    <StyledTableCell align="center">
                      {currentUser.firstname} {currentUser.lastname}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {currentUser.emailaddress}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      ${currentUser.accountbalance}
                    </StyledTableCell>
                  </StyledTableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <Grid container alignItems="center" justify="center">
          <Grid>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleLogout}
            >
              Logout
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
