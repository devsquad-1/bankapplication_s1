import React from "react";
import Login from "./components/Login";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import UserDashboard from "./components/UserDashboard";
import AdminDashboard from "./components/AdminDashboard";
import RegisterUser from "./components/RegisterUser";
import AdminLogin from "./components/AdminLogin";
import Logout from "./components/Logout";
import SuccessRegister from "./components/SuccessRegister";
import ForgotPassword from "./components/ForgotPassword";
import ResetPassword from "./components/ResetPassword";
import AdminLogout from "./components/AdminLogout";
import Navbar from "./components/NavBar";

function App() {
  return (
    <BrowserRouter>
      <Navbar>
        <Switch>
          <Route path="/login" component={Login} />
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route path="/admin" component={AdminLogin} />
          <Route path="/user-dashboard" component={UserDashboard} />
          <Route path="/admin-dashboard" component={AdminDashboard} />
          <Route path="/register" component={RegisterUser} />
          <Route path="/logout" component={Logout} />
          <Route path="/success" component={SuccessRegister} />
          <Route path="/forgotpassword" component={ForgotPassword} />
          <Route path="/resetpassword" component={ResetPassword} />
          <Route path="/admin-logout" component={AdminLogout} />
        </Switch>
      </Navbar>
    </BrowserRouter>
  );
}

export default App;
